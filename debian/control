Source: cadvisor
Section: devel
Priority: extra
Maintainer: pkg-go <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Dmitry Smirnov <onlyjob@debian.org>, Tim Potter <tpot@hpe.com>
Build-Depends: debhelper (>= 9), dh-systemd,
    dh-golang,
    go-bindata,
    golang-go,
    golang-dbus-dev,
    golang-gocapability-dev,
    golang-google-api-dev | golang-googlecode-p-google-api-go-client-dev,
    golang-github-aws-aws-sdk-go-dev (>= 1.0.7~),
    golang-google-cloud-compute-metadata-dev,
    golang-prometheus-client-dev,
    golang-procfs-dev,
    golang-protobuf-extensions-dev,
    golang-github-abbot-go-http-auth-dev,
    golang-github-beorn7-perks-dev,
    golang-github-coreos-go-systemd-dev,
    golang-github-docker-distribution-dev,
    golang-github-docker-docker-dev,
    golang-github-docker-engine-api-dev,
    golang-github-docker-go-units-dev,
    golang-gopkg-eapache-go-resiliency.v1-dev,
    golang-gopkg-eapache-queue.v1-dev,
    golang-github-fsouza-go-dockerclient-dev (>= 0.0+git20160316~),
    golang-github-garyburd-redigo-dev,
    golang-glog-dev | golang-github-golang-glog-dev,
    golang-goprotobuf-dev | golang-github-golang-goprotobuf-dev,
    golang-github-golang-mock-dev,
    golang-github-golang-snappy-dev,
    golang-github-go-ini-ini-dev,
    golang-github-influxdb-influxdb-dev (>= 0.12.0~),
    golang-github-jmespath-go-jmespath-dev,
    golang-github-klauspost-crc32-dev,
    golang-github-mrunalp-fileutils-dev,
    golang-pretty-dev | golang-github-kr-pretty-dev,
    golang-text-dev | golang-github-kr-text-dev,
    golang-github-seandolphin-bqschema-dev,
    golang-github-seccomp-libseccomp-golang-dev,
    golang-github-shopify-sarama-dev,
    golang-github-sirupsen-logrus-dev | golang-logrus-dev,
    golang-github-stretchr-testify-dev | golang-testify-dev,
    golang-objx-dev | golang-github-stretchr-objx-dev,
    golang-github-vishvananda-netlink-dev,
    golang-github-pborman-uuid-dev,
    golang-golang-x-exp-dev,
    golang-golang-x-net-dev,
    golang-golang-x-oauth2-dev,
    golang-gopkg-olivere-elastic.v2-dev,
    golang-google-grpc-dev,
    golang-go-zfs-dev,
    golang-github-coreos-pkg-dev,
    golang-github-blang-semver-dev,
    golang-github-docker-go-connections-dev,
Standards-Version: 3.9.8
Homepage: https://github.com/google/cadvisor
Vcs-Browser: https://salsa.debian.org/go-team/packages/cadvisor
Vcs-Git: https://salsa.debian.org/go-team/packages/cadvisor.git

Package: golang-github-google-cadvisor-dev
Architecture: all
Depends: ${misc:Depends},
    golang-go,
    golang-dbus-dev,
    golang-gocapability-dev,
    golang-google-api-dev | golang-googlecode-p-google-api-go-client-dev,
    golang-github-aws-aws-sdk-go-dev (>= 1.0.7~),
    golang-github-beorn7-perks-dev,
    golang-github-coreos-go-systemd-dev,
    golang-prometheus-client-dev,
    golang-procfs-dev,
    golang-protobuf-extensions-dev,
    golang-github-abbot-go-http-auth-dev,
    golang-github-docker-docker-dev,
    golang-github-docker-go-units-dev,
    golang-gopkg-eapache-go-resiliency.v1-dev,
    golang-gopkg-eapache-queue.v1-dev,
    golang-github-fsouza-go-dockerclient-dev,
    golang-github-garyburd-redigo-dev,
    golang-glog-dev | golang-github-golang-glog-dev,
    golang-goprotobuf-dev | golang-github-golang-goprotobuf-dev,
    golang-github-golang-mock-dev,
    golang-github-golang-snappy-dev,
    golang-github-go-ini-ini-dev,
    golang-github-influxdb-influxdb-dev (>= 0.12.0~),
    golang-github-jmespath-go-jmespath-dev,
    golang-github-klauspost-crc32-dev,
    golang-github-mrunalp-fileutils-dev,
    golang-pretty-dev | golang-github-kr-pretty-dev,
    golang-text-dev | golang-github-kr-text-dev,
    golang-github-seandolphin-bqschema-dev,
    golang-github-seccomp-libseccomp-golang-dev,
    golang-github-shopify-sarama-dev,
    golang-github-sirupsen-logrus-dev | golang-logrus-dev,
    golang-github-stretchr-testify-dev | golang-testify-dev,
    golang-objx-dev | golang-github-stretchr-objx-dev,
    golang-golang-x-exp-dev,
    golang-golang-x-net-dev,
    golang-golang-x-oauth2-dev,
    golang-gopkg-olivere-elastic.v2-dev,
    golang-google-grpc-dev,
    golang-go-zfs-dev,
    golang-github-coreos-pkg-dev,
    golang-github-blang-semver-dev,
    golang-github-docker-go-connections-dev
Description: analyze resource usage and performance of running containers
 cAdvisor (Container Advisor) provides container users an understanding of
 the resource usage and performance characteristics of their running
 containers.
 .
 cAdvisor has native support for Docker containers and should support just
 about any other container type out of the box.
 .
 cAdvisor also exposes container stats as Prometheus (http://prometheus.io)
 metrics.
 .
 This package provides golang library sources.

Package: cadvisor
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}, lsb-base, libjs-bootstrap, libjs-jquery (>= 1.11.3)
Built-Using: ${misc:Built-Using} ${my:Built-Using}
Description: analyze resource usage and performance characteristics of running containers
 cAdvisor (Container Advisor) provides container users an understanding of
 the resource usage and performance characteristics of their running
 containers.
 .
 cAdvisor has native support for Docker containers and should support just
 about any other container type out of the box.
 .
 cAdvisor also exposes container stats as Prometheus (http://prometheus.io)
 metrics.
 .
 This package provides daemon that collects, aggregates, processes, and
 exports information about running containers. Specifically, for each
 container it keeps resource isolation parameters, historical resource
 usage, histograms of complete historical resource usage and network
 statistics. This data is exported by container and machine-wide.
